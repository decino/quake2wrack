#define PROGRESS_WIDTH 70

char *GetFileExtension(int s, char *filename);
char *ToLowerCase(char *s);
void ProgressFinished();
void ShowProgress(long current, long max);
void GetEntriesFromTextFile(char *file, std::string *ents);
int CountEntriesInTextFile(char *file);