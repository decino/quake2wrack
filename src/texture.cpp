#include "global.h"

string GetWrackTextureCounterpart(string texture)
{
	for (int i = 0; i < NUM_TEXTURES; i += 2)
	{
		if (SharewareQuakeTextures[i] == texture)
			return SharewareQuakeTextures[i + 1];
	}
	return texture;
}