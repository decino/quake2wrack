#define WRACK_ANGLE (0xffffffff / 360)

bool IsConvertable(std::string data);
std::string GetWrackCounterpart(std::string data);
bool ShouldNotSpawn(entity_t entity);
char GetWrackThingDifficultyFlag(entity_t entity);
std::string UpgradeThingUsingSpawnflags(entity_t entity, std::string thing);

extern std::string *QuakeEntities;
extern int NUM_ENTITIES;