bool IsDroppedFile(int count);
bool IsValidBSPVersion(unsigned char version);
bool IsBSPFile(char *filename);