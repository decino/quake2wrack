#include "global.h"

bool IsConvertable(string data)
{
	for (int i = 0; i < NUM_ENTITIES; i += 2)
	{
		if (data == QuakeEntities[i])
			return true;
	}
	return false;
}

string GetWrackCounterpart(string data)
{
	for (int i = 0; i < NUM_ENTITIES; i += 2)
	{
		if (data == QuakeEntities[i])
			return QuakeEntities[i + 1];
	}
	return "UnknownThing";
}

bool ShouldNotSpawn(entity_t entity)
{
	// Crucified zombies shouldn't spawn
	if ((entity.classname == "monster_zombie") && (entity.spawnflags & 1))
		return true;
	// Deathmatch items shouldn't spawn
	if ((entity.spawnflags >= 1792) && (entity.spawnflags & 1792))
		return true;
	return false;
}

char GetWrackThingDifficultyFlag(entity_t entity)
{
	char flags = 0x0;

	if (entity.spawnflags & 256)
		flags |= 0x1;
	if (entity.spawnflags & 512)
		flags |= 0x2;
	if (entity.spawnflags & 1024)
		flags |= 0x4;
	return flags;
}

string UpgradeThingUsingSpawnflags(entity_t entity, string thing)
{
	if (entity.spawnflags & 1)
	{
		// Ammo upgrades
		if (thing == "Bullets")
			return "BulletBox";
		if (thing == "Shells")
			return "ShellBox";
		if (thing == "PulseCells")
			return "PulseCellBox";
		if (thing == "Rockets")
			return "RocketBox";

		// Health upgrades
		if (thing == "HealthBonusPlaced")
			return "MedkitPlaced";
	}
	if (entity.spawnflags & 2)
	{
		if (thing == "HealthBonusPlaced")
			return "MegaHealthPlaced";
	}
	return thing;
}