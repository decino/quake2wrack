#define BSP_ENTRIES 15

enum
{
	BSP_ENTITIES,
	BSP_PLANES,
	BSP_MIPTEX,
	BSP_VERTICES,
	BSP_VISILIST,
	BSP_NODES,
	BSP_TEXINFO,
	BSP_FACES,
	BSP_LIGHTMAPS,
	BSP_CLIPNODES,
	BSP_LEAVES,
	BSP_LFACES,
	BSP_EDGES,
	BSP_LEDGES,
	BSP_MODELS
};

typedef struct
{ 
	long offset;
	long size;
} 
dentry_t;

typedef struct                 
{ 
	long version;
	dentry_t lump[BSP_ENTRIES];
} 
dheader_t;

typedef float scalar_t;

typedef struct
{ 
	scalar_t x;
	scalar_t y;
	scalar_t z;
} 
vec3_t;

typedef struct
{ 
	vec3_t min;
	vec3_t max;
} 
boundbox_t;

typedef struct
{ 
	short min;
	short max;
} 
bboxshort_t;

typedef struct
{
	float x;
	float y;
	float z;
} 
vertex_t;

typedef struct
{ 
	unsigned short vertex0;
	unsigned short vertex1;
} 
edge_t;

typedef struct  
{ 
	unsigned short plane_id;
	unsigned short side;
	long ledge_id;
	unsigned short ledge_num;
	unsigned short texinfo_id;
	unsigned char typelight;
	unsigned char baselight;
	unsigned char light[2];
	long lightmap;
} 
face_t;

typedef struct
{
	std::string classname;
	short x;
	short y;
	short z;
	short angle;
	short light;
	short style;
	short spawnflags;
}
entity_t;

typedef struct
{ 
	vec3_t vectorS;
	scalar_t distS;
	vec3_t vectorT;
	scalar_t distT;
	unsigned long texture_id;
	unsigned long animated;
} 
surface_t;

typedef struct
{ 
	long numtex;
	long offset[256];
} 
mipheader_t;

typedef struct
{ 
	char name[16];
	unsigned long width;
	unsigned long height;
	unsigned long offset1;
	unsigned long offset2;
	unsigned long offset4;
	unsigned long offset8;
} 
miptex_t;

typedef struct
{
	short x;
	short y;
	short z;
}
wrackvertex_t;

typedef struct
{
	wrackvertex_t verts[64];
	bool skip[64];
	long num_verts;
	std::string texture;
}
wrackface_t;


