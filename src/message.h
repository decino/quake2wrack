enum 
{ 
	TITLE,
	TEXT,
	INFO,
	ERROR,
	NOTHING
};

char *MessageType(int type);
void Print(int type, char *msg);

void PrintEmptyLine();
void PrintTitle();
