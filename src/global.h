#include <algorithm>
#include <fstream>
#include <iostream>
#include <string>
#include <sstream>

#include "bsp.h"
#include "entity.h"
#include "convert.h"
#include "error.h"
#include "message.h"
#include "texture.h"
#include "utils.h"

using namespace std;

extern ifstream BSPFile;

#define BSP_VERSION 29
#define MAP_HEADER ('M' + ('M' << 8) + ('A' << 16) + ('P' << 24))

#define ENTITYLIST_FILENAME "EntityList.txt"
#define TEXTURELIST_FILENAME "TextureList.txt"