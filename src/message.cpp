#include "global.h"

char *MessageType(int type)
{
	switch (type)
	{
		case TITLE: return "* ";
		case TEXT: return "- ";
		case INFO: return "[INFO] ";
		case ERROR: return "[ERROR] ";
		case NOTHING: return "";
		default: return "";
	}
}

void Print(int type, char *msg)
{
	cout << MessageType(type) << msg << endl;
}

void PrintEmptyLine()
{
	Print(NOTHING, "");
}

void PrintTitle()
{
	Print(TITLE, "Quake2Wrack - (C) 2014 by decino");
	Print(TITLE, "Quake .bsp to Wrack .map converter");
	PrintEmptyLine();
}