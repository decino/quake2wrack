#include "global.h"

char *GetFileExtension(int s, char *filename)
{
	char *ret = new char[s];
	
	for (int i = 0; i <= s; i++)
	{
		ret[i] = filename[strlen(filename) - (s - i)];
	}
	return ret;
}

char *ToLowerCase(char *s)
{
	int i = 0;

	while (s[i])
	{
		s[i] = tolower(s[i]);
		i++;
	}
	return s;
}

void ProgressFinished()
{
	cout << '\r';

	for (int i = 0; i < PROGRESS_WIDTH; i++)
	{
		cout << ".";
	}
	cout << "Done!" << endl;
}

void ShowProgress(long current, long max)
{
	if (current % (max / PROGRESS_WIDTH + 1) != 0)
		return;
	cout << ".";
}

void GetEntriesFromTextFile(char *file, string *ents)
{
	ifstream in(file);
	string data;
	int index = 0;

	while (getline(in, data))
	{
		data.erase(remove_if(data.begin(), data.end(), isspace), data.end());

		if (count(data.begin(), data.end(), ',') > 0)
		{
			istringstream ss(data);
			string entry;

			while (getline(ss, entry, ','))
			{
				entry.erase(remove(entry.begin(), entry.end(), '\"'), entry.end());
				ents[index] = entry;
				index++;
			}
		}
	}
}

int CountEntriesInTextFile(char *file)
{
	ifstream in(file);
	string data;
	size_t n = 0;

	while (getline(in, data))
	{
		data.erase(remove_if(data.begin(), data.end(), isspace), data.end());
		n += count(data.begin(), data.end(), ',');
	}
	return n;
}