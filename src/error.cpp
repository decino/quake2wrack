#include "global.h"

bool IsDroppedFile(int count)
{
	if (count < 2)
	{
		Print(ERROR, "No file found. Please drag and drop the .bsp file to the .exe");
		cin.get();
		return false;
	}
	return true;
}

bool IsValidBSPVersion(unsigned char version)
{
	if (version != BSP_VERSION)
	{
		Print(ERROR, "Only .bsp files from the non-shareware Quake can be converted");
		cin.get();
		return false;
	}
	return true;
}

bool IsBSPFile(char *filename)
{
	if (strcmp(ToLowerCase(GetFileExtension(3, filename)), "bsp"))
	{
		Print(ERROR, "This is not a .bsp file");
		cin.get();
		return false;
	}
	return true;
}
