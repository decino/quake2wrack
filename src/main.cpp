#include "global.h"

unsigned char version;
ifstream BSPFile;

void OpenBSPFile(char *file)
{
	BSPFile.open(file, ios::binary);
	BSPFile.read(reinterpret_cast<char*>(&version), 1);
}

int main(int argc, char **argv)
{
	PrintTitle();

	if (!IsDroppedFile(argc) || !IsBSPFile(argv[1]))
		return EXIT_FAILURE;
	OpenBSPFile(argv[1]);

	if (!IsValidBSPVersion(version))
		return EXIT_FAILURE;

	Print(INFO, "File successfully loaded:");
	Print(INFO, argv[1]);
	PrintEmptyLine();
	Print(TEXT, "Press the ENTER key to start converting");
	cin.get();

	InitConverter();
	PrintEmptyLine();
	Print(TEXT, "Conversion successful");
	cin.get();
}