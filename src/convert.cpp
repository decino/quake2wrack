#include "global.h"

dheader_t dh;
vertex_t *vertex;
edge_t *edge;
face_t *face;
entity_t *entity;
surface_t *texinfo;
mipheader_t texture;
int *ledge;

ofstream MAPFile("Converted.map", ios::out | ios::binary);
long MAP_VERSION = 25;
long vert_count;
long face_count;
long entity_count;
long entity_buffer;

wrackface_t *wf;
long *num_verts_buffer;

int NUM_ENTITIES;
string *QuakeEntities;

int NUM_TEXTURES;
string *SharewareQuakeTextures;

void FillBytes(int num)
{
	bool empty = 0;

	for (int i = 0; i < num; i++)
	{
		MAPFile.write(reinterpret_cast<char*>(&empty), sizeof(bool));
	}
}

void CreateMapFile()
{
	// Map header and version
	MAPFile << "MMAP";
	MAPFile.write(reinterpret_cast<char*>(&MAP_VERSION), sizeof(long));
	FillBytes(4);

	// Sky
	short size = 4;
	MAPFile.write(reinterpret_cast<char*>(&size), sizeof(short));
	MAPFile << "E1L6";

	FillBytes(41);
	MAPFile.write(reinterpret_cast<char*>(&vert_count), sizeof(long));
	MAPFile.write(reinterpret_cast<char*>(&face_count), sizeof(long));
	FillBytes(8);
	MAPFile.write(reinterpret_cast<char*>(&entity_count), sizeof(long));

	/*cout << "Vertices: " << vert_count << endl;
	cout << "Faces: " << face_count << endl;
	cout << "Entities: " << entity_count << endl;*/
}

void BuildWrackVertices(int num_faces)
{
	Print(INFO, "Building vertices");

	for (int i = 0; i < num_faces; i++)
	{
		ShowProgress(i, num_faces);

		for (int j = 0; j < num_verts_buffer[i]; j++)
		{
			if (wf[i].skip[j])
				continue;

			MAPFile.write(reinterpret_cast<char*>(&wf[i].verts[j].x), sizeof(short));
			MAPFile.write(reinterpret_cast<char*>(&wf[i].verts[j].z), sizeof(short)); // Quake uses the z-axis as its vertical axis
			MAPFile.write(reinterpret_cast<char*>(&wf[i].verts[j].y), sizeof(short));
			FillBytes(16);
		}
    }
	ProgressFinished();
}

void BuildWrackFaces(int num_faces)
{
	long vert_index = 0;
	long scale  = 0x00003E00; // This is 0.125 in floating-point
	float friction = 1.0;
	short fog = 255;

	Print(INFO, "Building faces");

	for (int i = 0; i < num_faces; i++)
	{
		ShowProgress(i, num_faces);

		if (wf[i].num_verts == 0)
			continue;

		int max_verts = (vert_index + wf[i].num_verts);
		MAPFile.write(reinterpret_cast<char*>(&wf[i].num_verts), sizeof(long));

		while (vert_index < max_verts)
		{
			MAPFile.write(reinterpret_cast<char*>(&vert_index), sizeof(long));
			vert_index++;
		}
		wf[i].texture = GetWrackTextureCounterpart(wf[i].texture) + ".png";
		short size = wf[i].texture.size();

		// Face flags
		long flags = 0;

		if (wf[i].texture == "white.png")
			flags = 32;
		if (wf[i].texture == "trigger.png")
			flags = 12183;
		MAPFile.write(reinterpret_cast<char*>(&flags), sizeof(long));

		// Texture size and name
		MAPFile.write(reinterpret_cast<char*>(&size), sizeof(short));
		MAPFile << wf[i].texture;

		FillBytes(6);

		// Texture scale
		MAPFile.write(reinterpret_cast<char*>(&scale), sizeof(long));
		MAPFile.write(reinterpret_cast<char*>(&scale), sizeof(long));
		FillBytes(9);

		// Light
		unsigned char light = 120;
		MAPFile.write(reinterpret_cast<char*>(&light), sizeof(char));

		// Fog
		MAPFile.write(reinterpret_cast<char*>(&fog), sizeof(char));
		MAPFile.write(reinterpret_cast<char*>(&fog), sizeof(char));
		MAPFile.write(reinterpret_cast<char*>(&fog), sizeof(char));
		MAPFile.write(reinterpret_cast<char*>(&fog), sizeof(char));
		MAPFile.write(reinterpret_cast<char*>(&friction), sizeof(float));
		FillBytes(32);

		// Scale
		MAPFile.write(reinterpret_cast<char*>(&friction), sizeof(float));
		FillBytes(3);
	}
	ProgressFinished();
}

void ConvertEntities()
{
	char *str = new char[dh.lump[BSP_ENTITIES].size];
	BSPFile.seekg(dh.lump[BSP_ENTITIES].offset, ios::beg);
	BSPFile.read(str, dh.lump[BSP_ENTITIES].size);

	const char *name = "temp.txt";
	ofstream temp_out(name, ios::binary | ios::trunc);
	int num_entities = 0;

	Print(INFO, "Converting entities");

	for (int i = 0; i < dh.lump[BSP_ENTITIES].size; i++)
	{
		ShowProgress(i, dh.lump[BSP_ENTITIES].size);

		if (str[i] == '{')
			temp_out << "ENTITY_START" << endl;
		if (str[i] == '}')
		{
			temp_out << "ENTITY_END" << endl;
			num_entities++;
		}
		if (str[i] == ' ')
		{
			temp_out << endl;
			continue;
		}
		if (str[i] != '"' && str[i] != '{' && str[i] != '}')
			temp_out << str[i];
	}
	ifstream temp(name);
	string data;
	int index = 0;
	entity = new entity_t[num_entities];

	while (getline(temp, data))
	{
		entity[index].angle = 0;
		entity[index].light = 0;
		entity[index].spawnflags = 0;
		entity[index].style = 0;

		// We've found an entity
		if (data == "ENTITY_START")
		{
			while (getline(temp, data))
			{
				if (data == "classname")
				{
					getline(temp, data);
					entity[index].classname = data;
				}
				if (data == "origin")
				{
					getline(temp, data);
					entity[index].x = atoi(data.c_str()); // X
					getline(temp, data);
					entity[index].y = atoi(data.c_str()); // Y
					getline(temp, data);
					entity[index].z = atoi(data.c_str()); // Z
				}
				if (data == "angle")
				{
					getline(temp, data);
					entity[index].angle = (atoi(data.c_str()) * -1) + 90; // Wrack's angles are backwards
				}
				if (data == "light")
				{
					getline(temp, data);
					entity[index].light = atoi(data.c_str());
				}
				if (data == "style")
				{
					getline(temp, data);
					entity[index].style = atoi(data.c_str());
				}
				if (data == "spawnflags")
				{
					getline(temp, data);
					entity[index].spawnflags = atoi(data.c_str());
				}
				if (data == "ENTITY_END")
					break;
			}
			entity_count++;
			index++;
		}
	}
	temp_out.close();
	temp.close();
	remove(name);

	entity_buffer = entity_count;
	
	// Filter out unneeded entities and apply spawnflags/styles
	for (int i = 0; i < entity_buffer; i++)
	{
		if (!IsConvertable(entity[i].classname) || ShouldNotSpawn(entity[i]))
		{
			entity[i].classname = "UnknownThing";
			entity_count--;
			continue;
		}
	}
	ProgressFinished();
}

void BuildEntities()
{
	Print(INFO, "Building entities");

	for (int i = 0; i < entity_buffer; i++)
	{
		ShowProgress(i, entity_buffer);

		string thing = GetWrackCounterpart(entity[i].classname);
		thing = UpgradeThingUsingSpawnflags(entity[i], thing);
		short size = thing.size();
		long length = 39;

		if (thing == "UnknownThing")
			continue;

		float x = entity[i].x;
		float y = entity[i].y;
		float z = entity[i].z;
		unsigned long angle = (WRACK_ANGLE * entity[i].angle);
		char difficulty = GetWrackThingDifficultyFlag(entity[i]);
		char flags = 1;

		MAPFile.write(reinterpret_cast<char*>(&size), sizeof(short));
		MAPFile << thing;

		if (thing == "LightObject")
		{
			float colour = 1.0;
			length = 63;
			MAPFile.write(reinterpret_cast<char*>(&length), sizeof(long));

			MAPFile.write(reinterpret_cast<char*>(&x), sizeof(float)); // X
			MAPFile.write(reinterpret_cast<char*>(&z), sizeof(float)); // Y
			MAPFile.write(reinterpret_cast<char*>(&y), sizeof(float)); // Z
			FillBytes(27);

			MAPFile.write(reinterpret_cast<char*>(&colour), sizeof(float));
			MAPFile.write(reinterpret_cast<char*>(&colour), sizeof(float));
			MAPFile.write(reinterpret_cast<char*>(&colour), sizeof(float));
			FillBytes(3);

			if (entity[i].light == 0)
				entity[i].light = 200;

			char intensity = 63;
			float radius = (float)entity[i].light;
			bool type = 1;

			MAPFile.write(reinterpret_cast<char*>(&intensity), sizeof(bool));
			MAPFile.write(reinterpret_cast<char*>(&radius), sizeof(float));
			MAPFile.write(reinterpret_cast<char*>(&type), sizeof(bool));
			FillBytes(3);
		}
		else
		{
			MAPFile.write(reinterpret_cast<char*>(&length), sizeof(long));

			MAPFile.write(reinterpret_cast<char*>(&x), sizeof(float)); // X
			MAPFile.write(reinterpret_cast<char*>(&z), sizeof(float)); // Y
			MAPFile.write(reinterpret_cast<char*>(&y), sizeof(float)); // Z

			MAPFile.write(reinterpret_cast<char*>(&angle), sizeof(long)); // Angle
			FillBytes(4);
			MAPFile.write(reinterpret_cast<char*>(&difficulty), sizeof(bool));
			MAPFile.write(reinterpret_cast<char*>(&flags), sizeof(bool)); // Disable their idle sounds
			FillBytes(17);
		}
	}
	ProgressFinished();
}

void BuildWrackFinish()
{
	// Map script
	FillBytes(5);
}

void InitConverter()
{	
	BSPFile.seekg(0, ios::beg);
	BSPFile.read((char*)&dh, sizeof(dh));

	int num_vertices = dh.lump[BSP_VERTICES].size/sizeof(vertex_t);
	int num_edges = dh.lump[BSP_EDGES].size/sizeof(edge_t);
	int num_faces = dh.lump[BSP_FACES].size/sizeof(face_t);
	int num_ledges = dh.lump[BSP_LEDGES].size/sizeof(int);
	int num_texinfo = dh.lump[BSP_TEXINFO].size/sizeof(surface_t);
	int num_textures = dh.lump[BSP_MIPTEX].size/sizeof(miptex_t);

	vertex = new vertex_t[num_vertices];
	BSPFile.seekg(dh.lump[BSP_VERTICES].offset, ios::beg);
	BSPFile.read((char*)vertex, dh.lump[BSP_VERTICES].size);

	edge = new edge_t[num_edges];
	BSPFile.seekg(dh.lump[BSP_EDGES].offset, ios::beg);
	BSPFile.read((char*)edge, dh.lump[BSP_EDGES].size);

	face = new face_t[num_faces];
	BSPFile.seekg(dh.lump[BSP_FACES].offset, ios::beg);
	BSPFile.read((char*)face, dh.lump[BSP_FACES].size);

	ledge = new int[num_ledges];
	BSPFile.seekg(dh.lump[BSP_LEDGES].offset, ios::beg);
	BSPFile.read((char*)ledge, dh.lump[BSP_LEDGES].size);

	texinfo = new surface_t[num_texinfo];
	BSPFile.seekg(dh.lump[BSP_TEXINFO].offset, ios::beg);
	BSPFile.read((char*)texinfo, dh.lump[BSP_TEXINFO].size);

	texinfo = new surface_t[num_texinfo];
	BSPFile.seekg(dh.lump[BSP_TEXINFO].offset, ios::beg);
	BSPFile.read((char*)texinfo, dh.lump[BSP_TEXINFO].size);

	BSPFile.seekg(dh.lump[BSP_MIPTEX].offset, ios::beg);
	BSPFile.read((char*)&texture, sizeof(mipheader_t));

	wf = new wrackface_t[num_faces];
	num_verts_buffer = new long[num_faces];
	vert_count = 0;
	face_count = 0;
	entity_count = 0;

	Print(INFO, "Converting geometry");

	for (int j = 0; j < num_faces; j++)
	{
		wf[j].num_verts = 0;
		num_verts_buffer[j] = 0;

		ShowProgress(j, num_faces);

		long offset = (dh.lump[BSP_MIPTEX].offset + texture.offset[texinfo[face[j].texinfo_id].texture_id]) - 1;
		string tex = "";

		for (int i = offset; i < (offset + 16); i++)
		{
			BSPFile.seekg(i, ios::beg);

			if (BSPFile.get() == 0 && i > (offset + 1))
				break;
			tex += (char)BSPFile.get();
		}
		tex = tex.substr(0, tex.size() - 1);
		wf[j].texture = tex;

		for (int k = 0; k < face[j].ledge_num; k++)
		{
			int index = ledge[face[j].ledge_id + k];
			unsigned short vert_id = edge[index].vertex0;
			wf[j].skip[k] = false;

			if (index < 0)
				vert_id = edge[-index].vertex1;

			wf[j].verts[k].x = (short)vertex[vert_id].x;
			wf[j].verts[k].y = (short)vertex[vert_id].y;
			wf[j].verts[k].z = (short)vertex[vert_id].z;

			num_verts_buffer[j]++;
			wf[j].num_verts++;
			vert_count++;
		}

		// Check if multiple vertices form a line
		for (int k = 0; k < face[j].ledge_num; k++)
		{
			int causing_vert = (k + 1);
			int next_vert = (k + 2);

			if (next_vert >= face[j].ledge_num)
				next_vert = 0;
			if (causing_vert >= face[j].ledge_num)
			{
				causing_vert = 0;
				next_vert = 1;
			}
			wrackvertex_t *wv = new wrackvertex_t[2];
			double *vert = new double[3];

			wv[0].x = (wf[j].verts[causing_vert].x - wf[j].verts[k].x);
			wv[0].y = (wf[j].verts[causing_vert].y - wf[j].verts[k].y);
			wv[0].z = (wf[j].verts[causing_vert].z - wf[j].verts[k].z);
			double length = sqrt((wv[0].x * wv[0].x) + (wv[0].y * wv[0].y) + (wv[0].z * wv[0].z));

			vert[0] = (wv[0].x / length);
			vert[1] = (wv[0].y / length);
			vert[2] = (wv[0].z / length);

			wv[1].x = (wf[j].verts[next_vert].x - wf[j].verts[k].x);
			wv[1].y = (wf[j].verts[next_vert].y - wf[j].verts[k].y);
			wv[1].z = (wf[j].verts[next_vert].z - wf[j].verts[k].z);
			length = sqrt((wv[1].x * wv[1].x) + (wv[1].y * wv[1].y) + (wv[1].z * wv[1].z));

			if ((wf[j].verts[k].x + (vert[0] * length) == wf[j].verts[next_vert].x) && (wf[j].verts[k].y + (vert[1] * length) == wf[j].verts[next_vert].y) && (wf[j].verts[k].z + (vert[2] * length) == wf[j].verts[next_vert].z))
			{
				wf[j].skip[causing_vert] = true;
				wf[j].num_verts--;
				vert_count--;
			}
		}
		// Wrack can't load faces with less than 3 vertices, so skip this face entirely
		if (wf[j].num_verts < 3)
		{
			for (int i = 0; i < num_verts_buffer[j]; i++)
			{
				if (!wf[j].skip[i])
					wf[j].skip[i] = true;
			}
			vert_count -= wf[j].num_verts;
			wf[j].num_verts = 0;
			continue;
		}
		face_count++;
	}
	ProgressFinished();

	NUM_ENTITIES = (CountEntriesInTextFile(ENTITYLIST_FILENAME) * 2);
	QuakeEntities = new string[NUM_ENTITIES];
	GetEntriesFromTextFile(ENTITYLIST_FILENAME, QuakeEntities);

	NUM_TEXTURES = (CountEntriesInTextFile(TEXTURELIST_FILENAME) * 2);
	SharewareQuakeTextures = new string[NUM_TEXTURES];
	GetEntriesFromTextFile(TEXTURELIST_FILENAME, SharewareQuakeTextures);

	ConvertEntities();
	CreateMapFile();
	BuildWrackVertices(num_faces);
	BuildWrackFaces(num_faces);
	BuildEntities();
	BuildWrackFinish();
	MAPFile.close();
}