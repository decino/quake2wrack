Quake .bsp to Wrack .map converter by decino
============================================

NOTES:

- Newer .bsp formats from Quake II, Half-Life, etc. are not supported (yet).
- The converter has no problem with the geometry, but Wrack doesn't like the way the vertices are stored.
- This causes some broken faces which have to be fixed manually. 
- WrackEd finds these automatically and warns you whenever you save the map.
- Quake uses floats for its coordinates, Wrack uses shorts, causing some broken faces.
